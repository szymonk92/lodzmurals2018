package lodzmurals.pl.lodzmurals;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import lodzmurals.pl.lodzmurals.data.AuthorInfo;
import lodzmurals.pl.lodzmurals.data.MuralInfo;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link MainActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */

/*
TODO: https://developer.android.com/training/animation/screen-slide.html?

 */
public class ItemDetailFragment extends Fragment implements OnMapReadyCallback {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_SINGLE = "item_id";

    protected MapView mMapView;
    private GoogleMap mMap;

    private MuralInfo mItem;

    public static ItemDetailFragment newInstance(MuralInfo mural) {
        Log.d("ItemDetailFragmentnewI", String.valueOf(mural));
        final ItemDetailFragment f = new ItemDetailFragment();

        final Bundle args = new Bundle();
        args.putParcelable(ARG_ITEM_SINGLE, Parcels.wrap(mural));
        f.setArguments(args);

        return f;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_SINGLE)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = getArguments() != null ? Parcels.unwrap(getArguments().getParcelable(ARG_ITEM_SINGLE)) : null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);

        StringBuilder builder = new StringBuilder();
        if (mItem != null) {
            int size = mItem.authors.size();
            for (AuthorInfo author : mItem.authors) {
                if (size > 1) {
                    builder.append(author.name + "\n");
                    size --;
                } else {
                    builder.append(author.name);
                }
            }
            ((TextView) rootView.findViewById(R.id.artists)).setText("Created by: " + builder.toString());
            ((TextView) rootView.findViewById(R.id.street_headline)).setText(mItem.street);
            ImageView imageView = rootView.findViewById(R.id.muralView);
            Picasso.with(getContext())
                    .load(mItem.imgs.get(0).images)
                    .into(imageView);
        }

        mMapView = (MapView) rootView.findViewById(R.id.map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mMapView != null) {
            mMapView.onResume();
        }
    }

    @Override
    public void onPause() {
        if (mMapView != null) {
            mMapView.onPause();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (mMapView != null) {
            try {
                mMapView.onDestroy();
            } catch (NullPointerException e) {
                Log.e("MYAPP", "Error while attempting MapView.onDestroy(), ignoring exception", e);
            }
        }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null) {
            mMapView.onLowMemory();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mMapView != null) {
            mMapView.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLng latLng = new LatLng(mItem.coords.latitude, mItem.coords.longitude);
                mMap.addMarker(new MarkerOptions()
                        .position(latLng));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(12), 1000, null);
            }
        });


        /*
          .snippet("Population: 4,137,400")
          .icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow)));
         */

    }
}
