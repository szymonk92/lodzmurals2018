package lodzmurals.pl.lodzmurals;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.bugsnag.android.Bugsnag;

import org.parceler.Parcels;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lodzmurals.pl.lodzmurals.data.AuthorInfo;
import lodzmurals.pl.lodzmurals.data.ImageInfo;
import lodzmurals.pl.lodzmurals.data.MuralInfo;
import lodzmurals.pl.lodzmurals.data.ProjectInfo;
import lodzmurals.pl.lodzmurals.maps.MapsActivity;
import lodzmurals.pl.lodzmurals.utils.AppStatus;
import lodzmurals.pl.lodzmurals.utils.InternalStorage;

/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ItemDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */

/*
TODO: https://github.com/florent37/MaterialViewPager
https://github.com/tuesda/CircleRefreshLayout
loading: https://github.com/SaeedMasoumi/FAB-Loading
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private boolean mTwoPane; //Whether or not the activity is in two-pane mode, i.e. running on a tablet device.
    private List<MuralInfo> murals_g = new ArrayList<>();
    public static Map<String, MuralInfo> murals_map = new HashMap<>();

    private boolean doubleBackToExitPressedOnce;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        Toolbar toolbar = setToolbar();
        setNavigationDrawer(toolbar);

        refresh();

        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        Bugsnag.init(this); //Logs
    }

    private void refresh() {
        SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        refreshLayout.setOnRefreshListener(() -> {
            if (AppStatus.getInstance(getApplicationContext()).isOnline()) {
                getData();
                refreshLayout.setRefreshing(true);
            } else {
                connectToInternet();
            }
        });
    }

    @NonNull
    private Toolbar setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        return toolbar;
    }

    private void setNavigationDrawer(Toolbar toolbar) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MYAPP", "OnResume");

        getData();
    }

    private void getData() {
        if (murals_g != null && murals_g.isEmpty()) {
            if (AppStatus.getInstance(getApplicationContext()).isOnline()) {
                findViewById(R.id.noIntternetConnection).setVisibility(View.INVISIBLE);
                getDataFromInternet();
            } else {
                //findViewById(R.id.noIntternetConnection).setVisibility(View.VISIBLE);
                //findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                //Check every few seconds for connection
                connectToInternet();
            }
        }
    }

    private void getDataFromInternet() {
        Backendless.initApp(getApplicationContext(),
                getString(R.string.backendless_appId),
                getString(R.string.backendless_appKey));

        Backendless.Data.mapTableToClass("murals", MuralInfo.class);
        Backendless.Data.mapTableToClass("images", ImageInfo.class);
        Backendless.Data.mapTableToClass("authors", AuthorInfo.class);
        Backendless.Data.mapTableToClass("projects", ProjectInfo.class);

        Backendless.Persistence.of(MuralInfo.class).find(new AsyncCallback<List<MuralInfo>>() {
            @Override
            public void handleResponse(List<MuralInfo> murals) {
                Log.d("MYAPP", "Retrieved " + murals.size() + " objects");
                murals_g = murals;
                for (MuralInfo mural : murals) {
                    murals_map.put(mural.objectId, mural);
                }
                findViewById(R.id.loadingPanel).setVisibility(View.GONE);

                View recyclerView = findViewById(R.id.item_list);
                assert recyclerView != null;
                setupRecyclerView((RecyclerView) recyclerView);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e("MYAPP", "Server reported an error " + fault.getMessage());
            }
        });
    }

    private void cacheObjects() {
        try {
            InternalStorage.writeObject(this, "LI", murals_g);

            murals_g = (List<MuralInfo>) InternalStorage.readObject(this, "LI");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void connectToInternet() {
        Snackbar.make(findViewById(R.id.drawer_layout),
                R.string.no_internet, Snackbar.LENGTH_LONG)
                .setAction(R.string.connect, view -> {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS), 0);
                })
                .show();
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new MuralsRecyclerViewAdapter(this, murals_g, mTwoPane));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //NaviDrawer
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) { //NaviDrawer
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.murals) {

        } else if (id == R.id.map) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(MapsActivity.MURALS, Parcels.wrap(murals_g));
            Intent intent = new Intent(this, MapsActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.back_to_exit, Toast.LENGTH_SHORT).show();

        mHandler.postDelayed(mRunnable, 2000);
    }


}
