package lodzmurals.pl.lodzmurals;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.List;

import lodzmurals.pl.lodzmurals.data.MuralInfo;

public class MuralsRecyclerViewAdapter
        extends RecyclerView.Adapter<MuralsRecyclerViewAdapter.ViewHolder> {

    private final MainActivity mParentActivity;
    private final List<MuralInfo> mValues;
    private final boolean mTwoPane;

    MuralsRecyclerViewAdapter(MainActivity parent,
                              List<MuralInfo> items,
                              boolean twoPane) {
        mParentActivity = parent;
        mValues = items;
        mTwoPane = twoPane;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        //holder.mIdView.setText(mValues.get(position).objectId);
        holder.mContentView.setText(mValues.get(position).street);
        Picasso.with(mParentActivity)
                .load(mValues.get(position).imgs.get(0).images)
                .into(holder.mImage);


        holder.itemView.setTag(mValues.get(position));
        holder.itemView.setOnClickListener(view -> {
            MuralInfo item = (MuralInfo) view.getTag();
            if (mTwoPane) {
                Bundle arguments = new Bundle();
                arguments.putString(ItemDetailFragment.ARG_ITEM_SINGLE, item.objectId);
                arguments.putParcelable(ItemDetailActivity.ARG_ITEM, Parcels.wrap(mValues)); //not tested
                arguments.putInt(ItemDetailActivity.ITEM_POS, position); //not tested
                ItemDetailFragment fragment = new ItemDetailFragment();
                fragment.setArguments(arguments);
                mParentActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit();
            } else {
                Context context = view.getContext();
                Intent intent = new Intent(context, ItemDetailActivity.class);
                intent.putExtra(ItemDetailFragment.ARG_ITEM_SINGLE, item.objectId);
                intent.putExtra(ItemDetailActivity.ARG_ITEM, Parcels.wrap(mValues));
                intent.putExtra(ItemDetailActivity.ITEM_POS, position);


                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }

    //View of CardView
    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mIdView;
        final TextView mContentView;
        final ImageView mImage;

        ViewHolder(View view) {
            super(view);
            mIdView = (TextView) view.findViewById(R.id.id_text);
            mContentView = (TextView) view.findViewById(R.id.content);
            mImage = (ImageView) view.findViewById(R.id.bg_image);
        }
    }
}