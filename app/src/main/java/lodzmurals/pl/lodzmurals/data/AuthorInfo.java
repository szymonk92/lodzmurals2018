package lodzmurals.pl.lodzmurals.data;

import org.parceler.Parcel;

@Parcel(analyze = AuthorInfo.class)
public class AuthorInfo {

    public String name;
    public String country;
    public String website;
    public String description;

    public AuthorInfo() {
    }
}
