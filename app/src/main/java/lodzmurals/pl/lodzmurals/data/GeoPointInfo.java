package lodzmurals.pl.lodzmurals.data;

import org.parceler.Parcel;

@Parcel(analyze = GeoPointInfo.class)
public class GeoPointInfo {
    public double latitude;
    public double longitude;

    public GeoPointInfo() {
    }
}
