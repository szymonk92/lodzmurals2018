package lodzmurals.pl.lodzmurals.data;

import org.parceler.Parcel;

@Parcel(analyze = ImageInfo.class)
public class ImageInfo{

    public ImageInfo() {
    }

    public String images;

}
