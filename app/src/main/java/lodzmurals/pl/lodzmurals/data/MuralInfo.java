package lodzmurals.pl.lodzmurals.data;

import org.parceler.Parcel;

import java.util.List;

@Parcel(analyze = {MuralInfo.class})
public class MuralInfo{

    public String objectId;
    public String street;
    public List<ImageInfo> imgs;
    public int year;
    public List<AuthorInfo> authors;
    public boolean exists;
    public ProjectInfo projects;
    public GeoPointInfo coords;

    public MuralInfo() {
    }
}
