package lodzmurals.pl.lodzmurals.data;

import org.parceler.Parcel;

@Parcel(analyze = ProjectInfo.class)
public class ProjectInfo {

    String name;
    String description;

    public ProjectInfo() {
    }
}
