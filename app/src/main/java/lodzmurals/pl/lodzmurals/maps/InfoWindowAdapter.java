package lodzmurals.pl.lodzmurals.maps;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import lodzmurals.pl.lodzmurals.R;

public class InfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private View view;
    private Map<String, String> images = Collections.emptyMap();

    protected InfoWindowAdapter( Context context, Map<String, String> images) {

        this.images = images == null ? this.images : images;
        this.view = LayoutInflater.from(context).inflate(R.layout.infowindow_layout, null);
    }

    @Override
    public View getInfoWindow(Marker marker) {

        return null;
    }

    @Override
    public View getInfoContents(final Marker marker) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        if (viewHolder == null) {
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        }
        renderInfoWindow(marker, viewHolder);
        return view;
    }

    private void renderInfoWindow(Marker marker, ViewHolder viewHolder) {
        if (images.containsKey(marker.getId())) {
            Log.i("Map", marker.getId());
            Picasso.with(view.getContext())
                    .load(images.get(marker.getId()))
                    .resize(100, 100)
                    .into(viewHolder.profileIcon, new Callback() {
                        @Override
                        public void onSuccess() {
                            if (marker != null && marker.isInfoWindowShown()) {
                                marker.hideInfoWindow();
                                marker.showInfoWindow();
                            }
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }
        viewHolder.title.setText(marker.getTitle());
        viewHolder.snippet.setText(marker.getSnippet());
    }


    private static class ViewHolder {

        private CircleImageView profileIcon;
        private TextView title;
        private TextView snippet;

        public ViewHolder(View view) {
            profileIcon = (CircleImageView) view.findViewById(R.id.profile_image);
            title = (TextView) view.findViewById(R.id.marker_label);
            snippet = (TextView) view.findViewById(R.id.marker_label2);
        }
    }

}
