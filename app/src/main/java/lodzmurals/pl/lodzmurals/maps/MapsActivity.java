package lodzmurals.pl.lodzmurals.maps;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.parceler.Parcels;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lodzmurals.pl.lodzmurals.R;
import lodzmurals.pl.lodzmurals.data.MuralInfo;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public static final String MURALS = "murals";
    private List<MuralInfo> murals;
    private static final LatLngBounds LODZ = new LatLngBounds(
            new LatLng(51.727504, 19.405334), new LatLng(51.815319, 19.505241));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Get from Activity
        Bundle getBundle = this.getIntent().getExtras();
        murals = Parcels.unwrap(getBundle.getParcelable(MURALS));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(LODZ, 0));

        Map<String, String> images = createMarkers();
        mMap.setInfoWindowAdapter(new InfoWindowAdapter(this, images));
    }

    //https://stackoverflow.com/questions/18938187/add-an-image-from-url-into-custom-infowindow-google-maps-v2/22043781#22043781
    //http://yasirameen.com/2017/09/understading-google-maps-marker/
    private Map<String, String> createMarkers() {
        Map<String, String> images = new HashMap<>();
        if (murals != null && murals.size() > 0) {
            for (MuralInfo m : murals) {
                Marker marker = mMap.addMarker(
                        new MarkerOptions()
                                .position(new LatLng(m.coords.latitude, m.coords.longitude))
                                .title(m.street)
                                .snippet(m.authors.get(0).name));
//                                .icon(BitmapDescriptorFactory.fromBitmap(createUserBitmap())));
                images.put(marker.getId(), m.imgs.get(0).images);
            }
        }
    return images;
    }
}

